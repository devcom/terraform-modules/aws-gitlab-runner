resource "aws_s3_bucket" "cache" {
  bucket = var.gitlab_runner_cache_name
  acl    = "private"

  tags = merge(local.service_tags,
    { "Name" : var.gitlab_runner_cache_name,
  "Comments" : "Shared cache for Docker Machines created by gitlab-runner" })
}

